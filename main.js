const {
    getAccessToken,
    grantConsent,
    authorizeConsent,
    exchangeAuthorizationCodeForAccessToken,
    getAccounts,
    getPayees,
    billPay
} = require('./api_client');

module.exports.init = async function () {


    let accessToken, consentId, authorizationCode, accessToken2;

    let accounts, payees, billPayResponse;

    console.log('Get access token:');

    accessToken = await getAccessToken();

    console.log(accessToken);

    console.log('Get cnosent id:')

    consentId = await grantConsent(accessToken);

    console.log(consentId);

    console.log('Authorize consent:');

    authorizationCode = await authorizeConsent(consentId);

    console.log(authorizationCode);

    console.log('Exchange authorization code for access token:');

    accessToken2 = await exchangeAuthorizationCodeForAccessToken(authorizationCode);

    console.log(accessToken2);

    console.log('Get accounts:');

    accounts = await getAccounts(accessToken2);

    console.log(accounts);

    console.log('Get payees:');

    payees = await getPayees(accessToken2);

    console.log(payees);

    console.log('Bill pay:');

    billPayResponse = await billPay(accessToken2);

    console.log(billPayResponse);

  };
