const {
    getAccessToken,
    grantConsent,
    authorizeConsent,
    exchangeAuthorizationCodeForAccessToken,
    getAccounts,
    getPayees,
    billPay
} = require('../api_client');

let accessToken, consentId, authorizationCode, accessToken2;

beforeEach(async () => {
    accessToken = await getAccessToken();
    consentId = await grantConsent(accessToken);
    authorizationCode = await authorizeConsent(consentId);
    accessToken2 = await exchangeAuthorizationCodeForAccessToken(authorizationCode);
});

describe('getAccessToken', () => {
    it('should return an access token', () => {
        expect(accessToken).toBeDefined();
    });
});

describe('grantConsent', () => {
    it('should return a consent ID', () => {
        expect(consentId).toBeDefined();
    });
});

describe('authorizeConsent', () => {
    it('should return a redirect URL', () => {
        expect(authorizationCode).toBeDefined();
    });
});

describe('exchangeAuthorizationCodeForAccessToken', () => {
    it('should return an access token', () => {
        expect(accessToken2).toBeDefined();
    });
});

describe('getAccounts', () => {
    it('should return a list of accounts', async () => {
        const accounts = await getAccounts(accessToken2);
        expect(accounts).toBeDefined();
    });
});

describe('getPayees', () => {
    it('should return a list of payees', async () => {
        const payees = await getPayees(accessToken2);
        expect(payees).toBeDefined();
    });
});

describe('billPay', () => {
    it('should return a bill pay response', async () => {
        const billPayResponse = await billPay(accessToken2);
        expect(billPayResponse).toBeDefined();
    });
});
