const { Before, Given, When, Then } = require('cucumber');
var assert = require('cucumber-assert');

const {
    getAccessToken,
    grantConsent,
    authorizeConsent,
    exchangeAuthorizationCodeForAccessToken,
    getAccounts,
    getPayees,
    billPay
} = require('../../api_client');

let accessToken, consentId, authorizationCode, accessToken2;

let accessTokenError, consentIdError, authorizationCodeError, accessToken2Error;

let accounts, payees, billPayResponse;

let accountsError, payeesError, billPayResponseError;

Before(async () => {

    accessToken = await getAccessToken();
    consentId = await grantConsent(accessToken);

    try {
        authorizationCode = await authorizeConsent(consentId);
    }
    catch (error) {
        authorizationCodeError = error;
    }
    try {
        accessToken2 = await exchangeAuthorizationCodeForAccessToken(authorizationCode);
    }
    catch (error) {
        accessToken2Error = error;
    }

    try {
        accounts = await getAccounts(accessToken2);
    }
    catch (error) {
        accountsError = error;
    }

    try {
        payees = await getPayees(accessToken2);
    }
    catch (error) {
        payeesError = error;
    }

    try {
        billPayResponse = await billPay(accessToken2);
    }
    catch (error) {
        billPayResponseError = error;
    }

});

Given('I have a valid API access token with ReadAccountsDetail API consent.', function () {
    assert.equal(consentIdError, null, 'Expect consent ID error to be null');
    assert.equal(accessToken2Error, null, 'Expect access token error to be null');
});

When('I call the accounts API endpoint.', function () {
    assert.equal(accountsError, null, 'Expect accounts error to be null');
});

Then('I receive a list of all accounts included in my Bank of APIs test data.', function () {
    assert.notEqual(accounts, null, 'Expect accounts to not be null');
});

Given('I have a valid API access token with BillPayService API consent.', function () {
    assert.equal(consentIdError, null, 'Expect consent ID error to be null');
    assert.equal(accessToken2Error, null, 'Expect access token error to be null');
});

When('I call the payees API endpoint providing a valid account ID.', function () {
    assert.equal(payeesError, null, 'Expect payees error to be null');
});

Then('I receive a list of all payees associated with that account ID.', function () {
    assert.notEqual(payees, null, 'Expect payees to not be null');
});

When('I call the bill-pay-request API endpoint providing a valid payeeId ID and amount.', function () {
    assert.equal(billPayResponseError, null, 'Expect bill pay response error to be null');
});

Then('I receive a response containing a valid request referenceNumber.', function () {
    assert.notEqual(billPayResponse, null, 'Expect bill pay response to not be null');
});
