const fetch = require('node-fetch');
const qs = require('qs');
const https = require('https');

require('dotenv').config();

const CLIENT_ID = process.env.CLIENT_ID;
const CLIENT_SECRET = process.env.CLIENT_SECRET;
const REDIRECT_URL = process.env.REDIRECT_URL;
const PSU_USERNAME = process.env.PSU_USERNAME;
const TRANSACTIONS_ACCOUNT_ID = process.env.TRANSACTIONS_ACCOUNT_ID;
const PAYEE_ID = process.env.PAYEE_ID;

async function getAccessToken() {
    const headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
    }

    const payload = {
        client_id: CLIENT_ID,
        client_secret: CLIENT_SECRET,
        grant_type: 'client_credentials',
        scope: 'accounts',
    };

    try {
        const response = await fetch('https://ob.sandbox.natwest.com/token', {
            method: 'POST',
            body: qs.stringify(payload),
            headers: headers
        });

        if (!response.ok) {
            throw new Error(`Access token creation failed. Response from the server was: ${response.status}`);
        }

        const data = await response.json();
        return data.access_token;
    } catch (error) {
        throw error;
    }
}

async function grantConsent(accessToken) {
    const headers = {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${accessToken}`,
        "Host": "ob.sandbox.natwest.com",
        "Accept-Encoding": "gzip, deflate, br",
        "Connection": "keep-alive",
        "Cache-Control": "no-cache",
        "Accept": "*/*",
    };
    const payload = {
        "Data": {
            "Permissions": [
                "ReadAccountsDetail",
                "BillPayService",
            ]
        },
        "Risk": {},
    }

    try {
        const response = await fetch(
            "https://ob.sandbox.natwest.com/open-banking/v3.1/aisp/account-access-consents",
            {
                method: 'POST',
                body: JSON.stringify(payload),
                headers: headers
            }
        );

        if (!response.ok) {
            throw new Error(`Grant consent failed. Response from the server was: ${response.status}`);
        }

        const data = await response.json();
        return data.Data.ConsentId;
    } catch (error) {
        throw error;
    }
}

async function authorizeConsent(consentId) {
    const headers = {
        "Content-Type": "application/json",
        "Host": "ob.sandbox.natwest.com",
        "Accept-Encoding": "gzip, deflate, br",
        "Connection": "keep-alive",
        "Accept": "*/*",
    };

    const clientId = CLIENT_ID;
    const encodedRedirectUrl = encodeURIComponent(REDIRECT_URL);
    const psuUsername = PSU_USERNAME;

    const url = `https://api.sandbox.natwest.com/authorize?client_id=${clientId}&response_type=code id_token&scope=openid accounts&redirect_uri=${encodedRedirectUrl}&state=ABC&request=${consentId}&authorization_mode=AUTO_POSTMAN&authorization_username=${psuUsername}`;

    const agent = new https.Agent({
        rejectUnauthorized: false
    });

    try {
        const response = await fetch(url, { headers: headers, agent: agent });

        if (!response.ok) {
            const errorData = await response.json();
            throw new Error(`Authorize consent failed. Response from the server was: ${JSON.stringify(errorData)}`);
        }

        const responseData = await response.json();
        const code = responseData.redirectUri.split("#")[1].split("&")[0].replace("code=", "");
        return code;
    } catch (error) {
        throw error;
    }
}

async function exchangeAuthorizationCodeForAccessToken(authorizationCode) {
    const headers = {
        "Content-Type": "application/x-www-form-urlencoded",
        "Host": "ob.sandbox.natwest.com",
        "Accept-Encoding": "gzip, deflate, br",
        "Connection": "keep-alive",
        "Accept": "*/*",
    };

    const data = {
        "grant_type": "authorization_code",
        "client_id": CLIENT_ID,
        "client_secret": CLIENT_SECRET,
        "redirect_uri": REDIRECT_URL,
        "code": authorizationCode,
    };

    try {
        const response = await fetch(
            "https://ob.sandbox.natwest.com/token",
            {
                method: 'POST',
                body: qs.stringify(data),
                headers: headers
            }
        );

        if (!response.ok) {
            const errorData = await response.json();
            throw new Error(`Exchange authorization code for access token failed. Response from the server was: ${JSON.stringify(errorData)}`);
        }

        const responseData = await response.json();
        return responseData.access_token;
    } catch (error) {
        throw error;
    }
}

async function getAccounts(authorizedAccessToken) {
    const headers = {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${authorizedAccessToken}`,
        "Host": "ob.sandbox.natwest.com",
        "Accept-Encoding": "gzip, deflate, br",
        "Connection": "keep-alive",
        "Cache-Control": "no-cache",
        "Accept": "*/*",
    };

    try {
        const response = await fetch(
            "https://ob.sandbox.natwest.com/open-banking/v3.1/aisp/accounts",
            { headers: headers }
        );

        if (!response.ok) {
            throw new Error(`Get accounts failed. Response from the server was: ${await response.json()}`);
        }

        return await response.json();
    } catch (error) {
        throw error;
    }
}

async function getPayees(authorizedAccessToken) {
    const headers = {
        "Accept-Encoding": "gzip, deflate, br",
        "Connection": "keep-alive",
        "Accept": "*/*",
        "Authorization": `Bearer ${authorizedAccessToken}`,
    };

    try {
        const response = await fetch(
            `https://ob.sandbox.natwest.com/open-banking/v3.1/aisp/accounts/${TRANSACTIONS_ACCOUNT_ID}/payees`,
            { headers: headers }
        );

        if (!response.ok) {
            throw new Error(`Get payees failed. Response from the server was: ${await response.json()}`);
        }

        return await response.json();
    } catch (error) {
        throw error;
    }
}

async function billPay(authorizedAccessToken) {
    const headers = {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${authorizedAccessToken}`,
        "Host": "ob.sandbox.natwest.com",
        "Accept-Encoding": "gzip, deflate, br",
        "Connection": "keep-alive",
        "Accept": "*/*",
    };

    const payload = {
        "payeeId": PAYEE_ID,
        "amount": "17.93",
    };

    try {
        const response = await fetch(
            `https://ob.sandbox.natwest.com/open-banking/v3.1/aisp/accounts/${TRANSACTIONS_ACCOUNT_ID}/bill-pay-request`,
            { method: 'POST', body: JSON.stringify(payload), headers: headers }
        );

        if (!response.ok) {
            throw new Error(`Bill pay failed. Response from the server was: ${await response.json()}`);
        }

        return await response.json();
    } catch (error) {
        throw error;
    }
}

module.exports = { getAccessToken, grantConsent, authorizeConsent, exchangeAuthorizationCodeForAccessToken, getAccounts, getPayees, billPay };
